const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "*",
};

var corsOptions3 = {
  origin: ["http://localhost:4200"],
};

var corsOptions2 = {
  origin: ["http://localhost:4200", "http://localhost:4202"],
  method: "['get']",
};

var corsOptions4 = {
  origin: ["http://localhost:4200", "http://localhost:4203"],
  method: "['post']",
};

const whitelist = [
  "http://localhost:4200",
  "http://localhost:4202",
  "http://localhost:4204",
];
const corsOptions5 = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

const blacklist = [
  "http://localhost:4200",
  "http://localhost:4202",
  "http://localhost:4204",
];
const corsOptions6 = {
  origin: function (origin, callback) {
    if (blacklist.indexOf(origin) !== -1) {
      callback(new Error("Not allowed by CORS"));
    } else {
      callback(null, true);
    }
  },
};

app.use(cors(corsOptions4));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");

db.sequelize.sync();

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to M2iFormation application." });
});

require("./app/routes/turorial.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
